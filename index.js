function createElement(tag, attributes, ...children) {
  let element = document.createElementNS('http://www.w3.org/2000/svg', tag);

  if (attributes) {
    for (let [key, value] of Object.entries(attributes)) {
      element.setAttribute(key, value);
    }
  }

  for (let child of children) {
    element.appendChild(typeof child === 'string' ? document.createTextNode(child) : child);
  }

  return element;
}


exports.createElement = createElement;

