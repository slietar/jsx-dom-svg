let { createElement } = require('.');

exports.jsx = (tag, { children: child, ...attributes }) => {
  return createElement(tag, attributes, ...(child ? [child] : []));
};

exports.jsxs = (tag, { children, ...attributes }) => {
  return createElement(tag, attributes, ...children);
};

